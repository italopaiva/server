from rest_framework.response import Response
from rest_framework import status


def created(data):
    return Response(data, status=status.HTTP_201_CREATED)


def errors(errors):
    return Response(
        errors,
        status=status.HTTP_422_UNPROCESSABLE_ENTITY
    )


def accepted(data):
    return Response(data, status=status.HTTP_202_ACCEPTED)


def not_found(msg):
    return Response(msg, status=status.HTTP_404_NOT_FOUND)


def no_content():
    return Response(status=status.HTTP_204_NO_CONTENT)


def ok(data):
    return Response(data, status=status.HTTP_200_OK)


def not_acceptable(data):
    return Response(data, status=status.HTTP_406_NOT_ACCEPTABLE)
