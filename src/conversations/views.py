from django.contrib.auth import get_user_model
from django.http import Http404
from django.utils.translation import ugettext as _

from pentano import messages
from pentano import api_responses as response

from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .serializers import VoteSerializer, CommentSerializer
from .serializers import ConversationSerializer
from .models import Vote, Comment, Conversation
User = get_user_model()


def filter_params(params, allowed_params):
    return {
        param: value
        for param, value in params.items() if param in allowed_params
    }


class VoteView(APIView):
    resource_name = 'votes'
    serializer = VoteSerializer

    def get(self, request, comment_id):
        votes = Vote.objects.filter(comment__pk=comment_id)
        serializer = self.serializer(votes, many=True)
        return Response(serializer.data)

    def post(self, request, comment_id):
        try:
            comment = Comment.objects.get(pk=comment_id)
        except (Comment.DoesNotExist):
            raise Http404
        else:
            serializer = self.serializer(data=request.data)
            if serializer.is_valid():
                user = request.user
                existing_vote = Vote.objects.filter(
                    author__id=user.pk,
                    comment__id=comment_id
                ).first()
                if not existing_vote:
                    params = self.create_vote_params(serializer.data)
                    vote = Vote(**params)
                    vote.comment = comment
                    vote.conversation = comment.conversation
                    vote.author = user
                    vote.save()
                else:
                    existing_vote.value = serializer.data['value']
                    existing_vote.save()
                    vote = existing_vote
                return response.created(self.serializer(vote).data)
            else:
                return response.errors(serializer.errors)

    def create_vote_params(self, params):
        return filter_params(params, ['value'])


class CommentView(APIView):
    resource_name = 'comments'
    serializer = CommentSerializer

    def get(self, request, conversation_id):
        comments = Comment.objects.filter(conversation__pk=conversation_id)
        serializer = self.serializer(comments, many=True)
        return Response(serializer.data)

    def post(self, request, conversation_id):
        try:
            conversation = Conversation.objects.get(pk=conversation_id)
        except (Conversation.DoesNotExist):
            raise Http404
        else:
            serializer = self.serializer(data=request.data)
            if serializer.is_valid():
                params = self.create_comment_params(serializer.data)
                comment = Comment(**params)
                comment.conversation = conversation
                comment.author = request.user
                comment.save()
                return response.created(self.serializer(comment).data)
            else:
                return response.errors(serializer.errors)

    def create_comment_params(self, params):
        return filter_params(params, ['content'])


class CommentDetailView(APIView):
    resource_name = 'commentDetails'
    serializer = CommentSerializer

    def get(self, request, comment_id, format=None):
        comment = self.retrieve_comment(comment_id)
        return Response(self.serializer(comment).data,
                        status=status.HTTP_200_OK)

    def patch(self, request, comment_id, format=None):
        comment = self.retrieve_comment(comment_id)
        if comment_id == request.data['id']:
            if 'author' not in request.data:
                serializer = self.serializer(
                    comment, data=request.data,
                    partial=True
                )
                if serializer.is_valid():
                    serializer.save()
                    return response.accepted(self.serializer(comment).data)
                return response.errors(serializer.errors)
            return response.not_found(_(messages.AUTHOR_CANT_BE_CHANGED))
        return response.errors(_(messages.PARAMETER_NOT_CORRESPONDING))

    def delete(self, request, comment_id, format=None):
        comment = self.retrieve_comment(comment_id)
        comment.delete()
        return response.no_content()

    def retrieve_comment(self, comment_id):
        try:
            comment = Comment.objects.get(pk=comment_id)
            return comment
        except (Comment.DoesNotExist, Conversation.DoesNotExist):
            raise Http404


class ConversationView(APIView):
    resource_name = 'conversations'
    serializer = ConversationSerializer

    def get(self, request):
        conversations = Conversation.objects.all()
        serializer = self.serializer(conversations, many=True)
        return response.ok(serializer.data)

    def post(self, request):
        serializer = self.serializer(data=request.data)
        if serializer.is_valid():
            conversation = Conversation(**serializer.data)
            conversation.author = request.user
            conversation.save()
            return response.created(self.serializer(conversation).data)
        return response.errors(serializer.errors)


class ConversationDetailView(APIView):
    resource_name = 'conversationDetails'
    serializer = ConversationSerializer

    def get(self, request, conversation_id, format=None):
        conversation = self.retrieve_conversation(conversation_id)
        return response.ok(self.serializer(conversation).data)

    def patch(self, request, conversation_id, format=None):
        if conversation_id == request.data['id']:
            conversation = self.retrieve_conversation(conversation_id)
            if 'author' not in request.data:
                serializer = self.serializer(conversation, data=request.data,
                                             partial=True)
                if serializer.is_valid():
                    serializer.save()
                    updated_data = self.serializer(conversation).data
                    return response.accepted(updated_data)
                return response.errors(serializer.errors)
            return response.not_acceptable(_(messages.AUTHOR_CANT_BE_CHANGED))
        return response.errors(_(messages.PARAMETER_NOT_CORRESPONDING))

    def delete(self, request, conversation_id, format=None):
        conversation = self.retrieve_conversation(conversation_id)
        conversation.delete()
        return response.no_content()

    def retrieve_conversation(self, conversation_id):
        try:
            conversation = Conversation.objects.get(pk=conversation_id)
            return conversation
        except Conversation.DoesNotExist:
            raise Http404


@api_view()
def get_conversation_by_user(request, author_id):
    conversations = Conversation.objects.filter(author__id=author_id)
    serializer = ConversationSerializer(conversations, many=True)
    return response.ok(serializer.data)


@api_view()
def get_comment_by_user(request, author_id):
    comments = Comment.objects.filter(author__id=author_id)
    serializer = CommentSerializer(comments, many=True)
    return response.ok(serializer.data)


@api_view()
def get_vote_by_user(request, author_id):
    votes = Vote.objects.filter(author__id=author_id)
    serializer = VoteSerializer(votes, many=True)
    return response.ok(serializer.data)
