from django.contrib.auth import get_user_model
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django_extensions.db.models import TimeStampedModel

User = get_user_model()


class Conversation(TimeStampedModel):
    author = models.ForeignKey(User, related_name='owned_conversations')
    title = models.CharField(max_length=150)
    description = models.TextField(blank=False)
    active = models.BooleanField(default=True)
    n_comments = models.IntegerField(default=0)
    n_participants = models.IntegerField(default=0)


class Comment(TimeStampedModel):
    conversation = models.ForeignKey(Conversation, related_name='comments')
    author = models.ForeignKey(User, related_name='comments')
    content = models.CharField(max_length=140, blank=False)
    active = models.BooleanField(default=True)
    is_seed = models.BooleanField(default=False)
    anonymous = models.BooleanField(default=False)
    c_index = models.IntegerField(default=0)


class Vote(TimeStampedModel):
    author = models.ForeignKey(User, related_name='votes')
    conversation = models.ForeignKey(Conversation, related_name='votes')
    comment = models.ForeignKey(Comment, related_name='votes')
    value = models.IntegerField(
        blank=False,
        validators=[MinValueValidator(-1), MaxValueValidator(1)]
    )
    p_index = models.IntegerField(default=0)
    c_index = models.IntegerField(default=0)

    class Meta:
        # A user cannot vote in a comment of a conversation more than once
        unique_together = (
            'author', 'conversation', 'comment',
            'p_index', 'c_index'
        )


class Participant(TimeStampedModel):
    user = models.ForeignKey(User, related_name='participations')
    conversation = models.ForeignKey(Conversation, related_name='participants')
    p_index = models.IntegerField()

    class Meta:
        unique_together = (
            # A user cannot participate in a conversation twice
            ('user', 'conversation'),
            # A conversation must have only one user in an index
            ('conversation', 'p_index')
        )
