from __future__ import unicode_literals
from django.contrib import admin
from .models import Conversation, Vote, Comment

admin.site.register(Conversation)
admin.site.register(Vote)
admin.site.register(Comment)
