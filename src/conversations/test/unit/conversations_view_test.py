from conversations.models import Conversation
from conversations.serializers import ConversationSerializer
from conversations.views import ConversationDetailView, ConversationView
from conversations.views import get_conversation_by_user

from django.contrib.auth import get_user_model

from rest_framework.test import APIRequestFactory, force_authenticate
from rest_framework_jwt.settings import api_settings
from test.test_helper import *

User = get_user_model()

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


@pytest.fixture(scope="function")
def setup():
    mommy.make(Conversation)
    mommy.make(User)
    factory = APIRequestFactory()
    user = mommy.make(User)
    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return (factory, user, payload, token)


@pytest.mark.django_db
def test_get_all_conversations(setup):
    factory, user, payload, token = setup
    conversation_view = ConversationView.as_view()
    request = factory.get('/conversations/')
    force_authenticate(request, user=user, token=token)
    response = conversation_view(request)
    conversations = Conversation.objects.all()
    serializer = ConversationSerializer(conversations, many=True)
    assert response.data == serializer.data


@pytest.mark.django_db
def test_post_a_conversation(setup):
    factory, user, payload, token = setup
    data = '{"data": {"type": "conversations","attributes": \
            {"title": "Conversation test","description": "This is the \
            conversation for testing."},\
            "relationships": {"author": {"data": {"type": "User",\
            "id": "1"}}}}}'
    request = factory.post(
        '/conversations/', data,
        content_type='application/vnd.api+json'
    )
    conversation_view = ConversationView.as_view()
    force_authenticate(request, user=user, token=token)
    response = conversation_view(request)
    conversation = Conversation.objects.get(pk=2)
    serializer = ConversationSerializer(conversation)
    assert response.data == serializer.data


@pytest.mark.django_db
def test_post_without_title(setup):
    factory, user, payload, token = setup
    data = '{"data": {"type": "conversations","attributes": \
            {"description": "This is the conversation for testing."},\
            "relationships": {"author": {"data": \
            {"type": "User","id": "1"}}}}}'
    request = factory.post(
        '/conversations/', data,
        content_type='application/vnd.api+json'
    )
    conversation_view = ConversationView.as_view()
    force_authenticate(request, user=user, token=token)
    response = conversation_view(request)
    assert response.status_code == 422
    assert response.data == {"title": ["This field is required."]}


@pytest.mark.django_db
def test_post_with_an_empty_title(setup):
    factory, user, payload, token = setup
    data = '{"data": {"type": "conversations","attributes": \
            {"title": "", "description": "This is the \
            conversation for testing."},\
            "relationships": {"author": {"data": \
            {"type": "User","id": "1"}}}}}'
    request = factory.post(
        '/conversations/', data,
        content_type='application/vnd.api+json'
    )
    conversation_view = ConversationView.as_view()
    force_authenticate(request, user=user, token=token)
    response = conversation_view(request)
    assert response.status_code == 422
    assert response.data == {'title': ['This field may not be blank.']}


@pytest.mark.django_db
def test_post_without_description(setup):
    factory, user, payload, token = setup
    data = '{"data": {"type": "conversations","attributes": \
            {"title": "Conversation test"},\
            "relationships": {"author": {"data": \
            {"type": "User","id": "1"}}}}}'
    request = factory.post(
        '/conversations/', data,
        content_type='application/vnd.api+json',
        headers={'Authorization'}
    )
    conversation_view = ConversationView.as_view()
    force_authenticate(request, user=user, token=token)
    response = conversation_view(request)
    assert response.status_code == 422
    assert response.data == {"description": ["This field is required."]}


@pytest.mark.django_db
def test_post_with_an_empty_description(setup):
    factory, user, payload, token = setup
    data = '{"data": {"type": "conversations","attributes": \
            {"title": "Conversation test","description": ""},\
            "relationships": {"author": {"data": \
            {"type": "User","id": "1"}}}}}'
    request = factory.post(
        '/conversations/', data,
        content_type='application/vnd.api+json'
    )
    conversation_view = ConversationView.as_view()
    force_authenticate(request, user=user, token=token)
    response = conversation_view(request)
    assert response.status_code == 422
    assert response.data == {'description': ['This field may not be blank.']}


@pytest.mark.django_db
def test_get_conversation_by_id(setup):
    factory, user, payload, token = setup
    request = factory.get('/conversations/1')
    conversation_detail_view = ConversationDetailView.as_view()
    force_authenticate(request, user=user, token=token)
    response = conversation_detail_view(request, 1)
    conversation = Conversation.objects.get(pk=1)
    serializer = ConversationSerializer(conversation)
    assert response.data == serializer.data


@pytest.mark.django_db
def test_get_with_an_unknown_conversation(setup):
    factory, user, payload, token = setup
    request = factory.get('/conversations/4')
    conversation_detail_view = ConversationDetailView.as_view()
    force_authenticate(request, user=user, token=token)
    response = conversation_detail_view(request, 4)
    assert response.status_code == 404


@pytest.mark.django_db
def test_update_conversation_by_id(setup):
    factory, user, payload, token = setup
    data = '{"data": {"type": "conversationDetails", "id": "1", "attributes": \
            {"title": "Conversation test update"}}}'
    request = factory.patch(
        '/conversations/1', data,
        content_type='application/vnd.api+json'
    )
    conversation_detail_view = ConversationDetailView.as_view()
    force_authenticate(request, user=user, token=token)
    response = conversation_detail_view(request, "1")
    conversation = Conversation.objects.get(pk=1)
    serializer = ConversationSerializer(conversation)
    assert response.data == serializer.data
    assert response.data['title'] == "Conversation test update"


@pytest.mark.django_db
def test_update_sending_the_user(setup):
    factory, user, payload, token = setup
    data = '{"data": {"type": "conversationDetails","id": "1", "attributes": \
            {"description": "This is the conversation for testing."},\
            "relationships": {"author": {"data": \
            {"type": "User","id": "1"}}}}}'
    request = factory.patch(
        '/conversations/1', data,
        content_type='application/vnd.api+json'
    )
    conversation_detail_view = ConversationDetailView.as_view()
    force_authenticate(request, user=user, token=token)
    response = conversation_detail_view(request, "1")
    assert response.status_code == 406


@pytest.mark.django_db
def test_update_with_an_unknown_conversation(setup):
    factory, user, payload, token = setup
    data = '{"data": {"type": "conversationDetails","id": "4", "attributes": \
            {"description": "This is the conversation for testing."}}}'
    request = factory.patch(
        '/conversations/4', data,
        content_type='application/vnd.api+json'
    )
    conversation_detail_view = ConversationDetailView.as_view()
    force_authenticate(request, user=user, token=token)
    response = conversation_detail_view(request, "4")
    assert response.status_code == 404


@pytest.mark.django_db
def test_delete_conversation_by_id(setup):
    factory, user, payload, token = setup
    request = factory.delete('/conversations/1')
    conversation_detail_view = ConversationDetailView.as_view()
    force_authenticate(request, user=user, token=token)
    response = conversation_detail_view(request, "1")
    assert response.status_code == 204
    conversations = Conversation.objects.all()
    assert conversations.count() == 0


@pytest.mark.django_db
def test_delete_with_an_unknown_conversation(setup):
    factory, user, payload, token = setup
    request = factory.delete('/conversations/4')
    conversation_detail_view = ConversationDetailView.as_view()
    force_authenticate(request, user=user, token=token)
    response = conversation_detail_view(request, "4")
    assert response.status_code == 404


@pytest.mark.django_db
def test_get_conversation_by_user(setup):
    factory, user, payload, token = setup
    conversation = Conversation.objects.get(pk=1)
    user = User.objects.get(pk=1)
    conversation.author = user
    conversation.save()
    url = '/conversations/author/%s' % user.pk
    request = factory.get(url)
    force_authenticate(request, user=user, token=token)
    response = get_conversation_by_user(request, user.pk)
    conversations = Conversation.objects.all()
    serializer = ConversationSerializer(conversations, many=True)
    assert response.data == serializer.data
