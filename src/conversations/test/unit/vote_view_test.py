from conversations.models import Comment, Vote
from conversations.serializers import VoteSerializer
from conversations.views import VoteView, get_vote_by_user

from django.contrib.auth import get_user_model

from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate
from rest_framework_jwt.settings import api_settings
from test.test_helper import *

User = get_user_model()
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


@pytest.fixture(scope="function")
def setup():
    comment = mommy.make(Comment)
    factory = APIRequestFactory()
    user = mommy.make(User)
    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return (factory, user, payload, token, comment)


@pytest.mark.django_db
def test_get_all_votes_of_a_comment(setup):
    factory, user, payload, token, comment = setup
    vote_view = VoteView.as_view()
    url = '/conversations/comments/%s/votes' % comment.pk
    request = factory.get(url)
    force_authenticate(request, user=user, token=token)
    response = vote_view(request, comment.pk)
    votes = Vote.objects.filter(comment__pk=comment.pk)
    serializer = VoteSerializer(votes, many=True)
    assert response.status_code == status.HTTP_200_OK
    assert response.data == serializer.data


def create_vote_json(value, author_id):
    return '''
    {
        "data": {
            "type": "votes",
            "attributes": {
                "value": "%s"
            },
            "relationships": {
                "author": {
                    "data": {
                        "type": "User",
                        "id": "%s"

                    }
                }
            }
        }
    }
    ''' % (value, author_id)


@pytest.mark.django_db
def test_create_new_valid_vote_for_comment(setup):
    factory, user, payload, token, comment = setup
    url = '/conversations/comments/%s/votes' % comment.pk
    data = create_vote_json('1', user.pk)
    request = factory.post(
        url, data, content_type='application/vnd.api+json'
    )
    vote_view = VoteView.as_view()
    force_authenticate(request, user=user, token=token)
    response = vote_view(request, comment.pk)
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.django_db
def test_create_vote_for_nonexistent_comment(setup):
    factory, user, payload, token, comment = setup
    invalid_comment_id = 123880
    url = '/conversations/comments/%s/votes' % invalid_comment_id
    data = create_vote_json('0', user.pk)
    request = factory.post(
        url, data, content_type='application/vnd.api+json'
    )
    vote_view = VoteView.as_view()
    force_authenticate(request, user=user, token=token)
    response = vote_view(request, invalid_comment_id)
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_create_vote_with_invalid_vote(setup):
    factory, user, payload, token, comment = setup
    url = '/conversations/comments/%s/votes' % comment.pk
    invalid_vote_value = '2'
    data = create_vote_json(invalid_vote_value, user.pk)
    request = factory.post(
        url, data, content_type='application/vnd.api+json'
    )
    vote_view = VoteView.as_view()
    force_authenticate(request, user=user, token=token)
    response = vote_view(request, comment.pk)
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.django_db
def test_create_existent_vote_only_update_its_value(setup):
    factory, user, payload, token, comment = setup
    url = '/conversations/comments/%s/votes' % comment.pk
    old_vote_value = 0
    previous_vote = mommy.make(
        Vote, author=user, comment=comment, value=old_vote_value
    )
    new_vote_value = 1
    data = create_vote_json(new_vote_value, user.pk)
    request = factory.post(
        url, data, content_type='application/vnd.api+json'
    )
    vote_view = VoteView.as_view()
    force_authenticate(request, user=user, token=token)
    response = vote_view(request, comment.pk)
    assert response.status_code == status.HTTP_201_CREATED
    assert previous_vote.value == old_vote_value
    previous_vote.refresh_from_db()
    assert previous_vote.value == new_vote_value


def test_create_vote_params_filtering():
    vote_view = VoteView()
    params = {
        'value': 'This is an allowed param for creation',
        'active': 'This is not an allowed param for creation',
        'another_param': 'This is not an allowed param for creation',
    }
    filtered_params = vote_view.create_vote_params(params)
    # Only the 'content' param is allowed
    assert len(filtered_params) == 1
    assert 'value' in filtered_params


@pytest.mark.django_db
def test_get_vote_by_user(setup):
    factory, user, payload, token, comment = setup
    vote = mommy.make(Vote)
    url = '/conversations/author/%s/votes' % vote.author.pk
    request = factory.get(url)
    force_authenticate(request, user=user, token=token)
    response = get_vote_by_user(request, vote.author.pk)
    votes = Vote.objects.all()
    serializer = VoteSerializer(votes, many=True)
    assert response.data == serializer.data
