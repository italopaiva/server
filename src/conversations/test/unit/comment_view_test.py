from conversations.models import Comment, Conversation
from conversations.serializers import CommentSerializer
from conversations.views import CommentView, CommentDetailView
from conversations.views import get_comment_by_user

from django.contrib.auth import get_user_model

from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate
from rest_framework_jwt.settings import api_settings
from test.test_helper import *

User = get_user_model()
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


@pytest.fixture(scope="function")
def setup():
    conversation = mommy.make(Conversation)
    factory = APIRequestFactory()
    user = mommy.make(User)
    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return (factory, user, payload, token, conversation)


@pytest.mark.django_db
def test_get_all_comments_of_a_conversation(setup):
    factory, user, payload, token, conversation = setup
    comment_view = CommentView.as_view()
    url = '/conversations/%s/comments' % conversation.pk
    request = factory.get(url)
    force_authenticate(request, user=user, token=token)
    response = comment_view(request, conversation.pk)
    comments = Comment.objects.filter(conversation__pk=conversation.pk)
    serializer = CommentSerializer(comments, many=True)
    assert response.data == serializer.data


def create_comment_json(content, author_id):
    return '''
    {
        "data": {
            "type": "comments",
            "attributes": {
                "content": "%s"
            },
            "relationships": {
                "author": {
                    "data": {
                        "type": "User",
                        "id": "%s"

                    }
                }
            }
        }
    }
    ''' % (content, author_id)


@pytest.mark.django_db
def test_create_new_valid_comment_for_conversation(setup):
    factory, user, payload, token, conversation = setup
    url = '/conversations/%s/comments' % conversation.pk
    author = mommy.make(User)
    data = create_comment_json('Test comment', author.pk)
    request = factory.post(
        url, data, content_type='application/vnd.api+json'
    )
    comment_view = CommentView.as_view()
    force_authenticate(request, user=user, token=token)
    response = comment_view(request, conversation.pk)
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.django_db
def test_create_new_comment_for_nonexistent_conversation(setup):
    factory, user, payload, token, conversation = setup
    invalid_conversation_id = "1090290"  # Arbitrary nonesxistent id
    url = '/conversations/%s/comments' % invalid_conversation_id
    author = mommy.make(User)
    data = create_comment_json('Test comment', author.pk)
    request = factory.post(
        url, data, content_type='application/vnd.api+json'
    )
    comment_view = CommentView.as_view()
    force_authenticate(request, user=user, token=token)
    response = comment_view(request, invalid_conversation_id)
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_create_new_comment_with_blank_content(setup):
    factory, user, payload, token, conversation = setup
    url = '/conversations/%s/comments' % conversation.pk
    author = mommy.make(User)
    data = create_comment_json('', author.pk)
    request = factory.post(
        url, data, content_type='application/vnd.api+json'
    )
    comment_view = CommentView.as_view()
    force_authenticate(request, user=user, token=token)
    response = comment_view(request, conversation.pk)
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


def test_create_comment_params_filtering():
    comment_view = CommentView()
    params = {
        'content': 'This is an allowed param for creation',
        'active': 'This is not an allowed param for creation',
        'another_param': 'This is not an allowed param for creation',
    }
    filtered_params = comment_view.create_comment_params(params)
    # Only the 'content' param is allowed
    assert len(filtered_params) == 1
    assert 'content' in filtered_params


def create_comment_details_json(content):
    return '''
    {
        "data": {
            "type": "commentDetails",
            "id": "1",
            "attributes": {
                "content": "%s"
            }
        }
    }
    ''' % (content)


@pytest.mark.django_db
def test_get_comment_by_id(setup):
    factory, user, payload, token, conversation = setup
    comment = mommy.make(Comment)
    url = '/conversations/comments/%s' % comment.pk
    request = factory.get(url)
    comment_detail_view = CommentDetailView.as_view()
    force_authenticate(request, user=user, token=token)
    response = comment_detail_view(request, comment.pk)
    serializer = CommentSerializer(comment)
    assert response.data == serializer.data


@pytest.mark.django_db
def test_get_nonexistent_comment_by_id(setup):
    factory, user, payload, token, conversation = setup
    mommy.make(Comment)
    invalid_id = "5"
    url = '/conversations/comments/%s' % invalid_id
    request = factory.get(url)
    comment_detail_view = CommentDetailView.as_view()
    force_authenticate(request, user=user, token=token)
    response = comment_detail_view(request, invalid_id)
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_update_comment(setup):
    factory, user, payload, token, conversation = setup
    comment = mommy.make(Comment)
    url = '/conversations/comments/%s' % comment.pk
    data = create_comment_details_json("teste comentário")
    request = factory.patch(url, data, content_type='application/vnd.api+json')
    comment_detail_view = CommentDetailView.as_view()
    force_authenticate(request, user=user, token=token)
    response = comment_detail_view(request, str(comment.pk))
    comment = Comment.objects.get(pk=comment.pk)
    serializer = CommentSerializer(comment)
    assert response.data == serializer.data


@pytest.mark.django_db
def test_update_comment_content_empty(setup):
    factory, user, payload, token, conversation = setup
    comment = mommy.make(Comment)
    url = '/conversations/comments/%s' % comment.pk
    data = create_comment_details_json("")
    request = factory.patch(url, data, content_type='application/vnd.api+json')
    comment_detail_view = CommentDetailView.as_view()
    force_authenticate(request, user=user, token=token)
    response = comment_detail_view(request, str(comment.pk))
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert response.data == {'content': ['This field may not be blank.']}


@pytest.mark.django_db
def test_delete_comment(setup):
    factory, user, payload, token, conversation = setup
    comment = mommy.make(Comment)
    url = '/conversations/comments/%s' % comment.pk
    request = factory.delete(url)
    comment_detail_view = CommentDetailView.as_view()
    force_authenticate(request, user=user, token=token)
    response = comment_detail_view(request, comment.pk)
    assert response.status_code == 204
    comments = Comment.objects.all()
    assert comments.count() == 0


@pytest.mark.django_db
def test_get_comment_by_user(setup):
    factory, user, payload, token, conversation = setup
    comment = mommy.make(Comment)
    url = '/conversations/author/%s/comments' % comment.author.pk
    request = factory.get(url)
    force_authenticate(request, user=user, token=token)
    response = get_comment_by_user(request, comment.author.pk)
    comments = Comment.objects.all()
    serializer = CommentSerializer(comments, many=True)
    assert response.data == serializer.data
