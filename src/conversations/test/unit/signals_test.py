from django.conf import settings

from conversations.signals import *
from conversations.models import Conversation, Comment, Vote
from test.test_helper import *

@pytest.fixture(scope="function")
def setup(connect_signals):
    conversation = mommy.make(Conversation)
    user = mommy.make(settings.AUTH_USER_MODEL)
    comment = mommy.make(
        'conversations.Comment',
        conversation=conversation,
        author=user,
    )
    return (conversation, user, comment)


@pytest.mark.django_db
def test_new_comment_should_increase_increase_n_participants(setup):
    conversation, user, comment = setup
    n_participants = conversation.n_participants
    new_user = mommy.make(settings.AUTH_USER_MODEL)

    comment = mommy.make(
        'conversations.Comment',
        conversation=conversation,
        author=new_user,
    )

    assert n_participants + 1 == conversation.n_participants


@pytest.mark.django_db
def test_new_comments_should_increase_n_comments(setup):
    conversation, user, comment = setup
    n_comments = conversation.n_comments

    comment = mommy.make(
        'conversations.Comment',
        conversation=conversation,
        author=user,
    )

    comment2 = mommy.make(
        'conversations.Comment',
        conversation=conversation,
        author=user,
    )

    assert n_comments + 2 == conversation.n_comments


@pytest.mark.django_db
def test_keep_n_participants_when_old_participant_make_new_comment(setup):
    conversation, user, comment = setup
    n_participants = conversation.n_participants

    comment = mommy.make(
        'conversations.Comment',
        conversation=conversation,
        author=user,
    )

    assert n_participants == conversation.n_participants


@pytest.mark.django_db
def test_assign_correct_index_for_new_comments(setup):
    conversation, user, comment = setup

    comment1 = mommy.make(
        'conversations.Comment',
        conversation=conversation,
        author=user,
    )

    comment2 = mommy.make(
        'conversations.Comment',
        conversation=conversation,
        author=user,
    )

    assert comment1.c_index == 1
    assert comment2.c_index == 2


@pytest.mark.django_db
def test_comment_index_should_not_change_when_editing_existent_comment(setup):
    conversation, user, comment = setup
    c_index = comment.c_index
    comment.content = "New content"
    comment.save()
    comment.refresh_from_db()

    assert c_index == comment.c_index


@pytest.mark.django_db
def test_new_vote_should_increase_n_participants(setup):
    conversation, user, comment = setup
    n_participants = conversation.n_participants
    new_user = mommy.make(settings.AUTH_USER_MODEL)

    vote = mommy.make(
        'conversations.Vote',
        conversation=conversation,
        author=new_user,
        comment=comment,
        value=1,
    )

    assert n_participants + 1 == conversation.n_participants


@pytest.mark.django_db
def test_keep_n_participants_when_old_participant_make_new_vote(setup):
    conversation, user, comment = setup
    n_participants = conversation.n_participants

    vote = mommy.make(
        'conversations.Vote',
        conversation=conversation,
        author=user,
        comment=comment,
        value=1,
    )

    assert n_participants == conversation.n_participants


@pytest.mark.django_db
def test_new_vote_should_keep_comment_index(setup):
    conversation, user, comment = setup
    new_user = mommy.make(settings.AUTH_USER_MODEL)

    vote = mommy.make(
        'conversations.Vote',
        conversation=conversation,
        author=user,
        comment=comment,
        value=1,
    )

    comment2 = mommy.make(
        'conversations.Comment',
        conversation=conversation,
        author=user,
    )

    vote2 = mommy.make(
        'conversations.Vote',
        conversation=conversation,
        author=new_user,
        comment=comment2,
        value=-1,
    )

    assert vote.c_index == comment.c_index
    assert vote2.c_index == comment2.c_index

@pytest.mark.django_db
def test_new_vote_should_keep_participant_index(setup):
    conversation, user, comment = setup
    new_user = mommy.make(settings.AUTH_USER_MODEL)

    vote = mommy.make(
        'conversations.Vote',
        conversation=conversation,
        author=user,
        comment=comment,
        value=1,
    )

    comment2 = mommy.make(
        'conversations.Comment',
        conversation=conversation,
        author=user,
    )

    vote2 = mommy.make(
        'conversations.Vote',
        conversation=conversation,
        author=new_user,
        comment=comment2,
        value=-1,
    )

    assert vote.p_index == 0
    assert vote2.p_index == 1
