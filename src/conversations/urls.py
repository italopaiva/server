from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.ConversationView.as_view(), name="conversations"),
    url(r'^(?P<conversation_id>[0-9]+)$',
        views.ConversationDetailView.as_view(), name="conversation"),
    url(
        r'^authors/(?P<author_id>[0-9]+)$',
        views.get_conversation_by_user,
        name="conversations_by_user"
    ),
    url(
        r'^(?P<conversation_id>[0-9]+)/comments$',
        views.CommentView.as_view(),
        name="comments"
    ),
    url(
        r'^comments/(?P<comment_id>[0-9]+)$',
        views.CommentDetailView.as_view(),
        name="comment"
    ),
    url(
        r'^authors/(?P<author_id>[0-9]+)/comments$',
        views.get_comment_by_user,
        name="comment_by_user"
    ),
    url(
        r'^comments/(?P<comment_id>[0-9]+)/votes$',
        views.VoteView.as_view(),
        name="votes"
    ),
    url(
        r'^authors/(?P<author_id>[0-9]+)/votes$',
        views.get_vote_by_user,
        name="vote_by_user"
    )
]
