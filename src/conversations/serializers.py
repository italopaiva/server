from .models import Conversation, Comment, Vote
from rest_framework import serializers
from django.contrib.auth import get_user_model

User = get_user_model()


class AuthorSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'name')


class ConversationSerializer(serializers.ModelSerializer):
    author = AuthorSerializer(read_only=True)

    class Meta:
        model = Conversation
        fields = '__all__'


class CommentSerializer(serializers.ModelSerializer):
    conversation = ConversationSerializer(read_only=True)
    author = AuthorSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = '__all__'


class VoteSerializer(serializers.ModelSerializer):
    conversation = ConversationSerializer(read_only=True)
    author = AuthorSerializer(read_only=True)
    comment = CommentSerializer(read_only=True)

    class Meta:
        model = Vote
        fields = '__all__'
