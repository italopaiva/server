# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-08 18:15
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('clusters', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cluster',
            name='centroids',
            field=jsonfield.fields.JSONField(blank=True),
        ),
        migrations.AlterField(
            model_name='cluster',
            name='n_clusters',
            field=models.IntegerField(blank=True),
        ),
        migrations.AlterField(
            model_name='cluster',
            name='silhouette',
            field=models.FloatField(blank=True),
        ),
    ]
