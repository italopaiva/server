from accounts.serializers import UserSerializer
from accounts.views import UserDetailView, create_user, get_users

from django.contrib.auth import get_user_model
from pentano import messages
from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate
from rest_framework_jwt.settings import api_settings
from test.test_helper import *

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

User = get_user_model()


@pytest.fixture(scope="function")
def factory_setup():
    user = mommy.make(User)
    factory = APIRequestFactory()
    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return (factory, user, payload, token)


@pytest.fixture(scope="module")
def user_detail_view():
    return UserDetailView.as_view()


@pytest.mark.django_db
def test_get_all_users(factory_setup):
    factory, user, payload, token = factory_setup
    mommy.make(User)
    users = User.objects.all()
    all_users = UserSerializer(users, many=True)
    request = factory.get('/users/')
    force_authenticate(request, user=user, token=token)
    response = get_users(request)
    assert response.data == all_users.data
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_get_user_by_id(factory_setup, user_detail_view):
    factory, user, payload, token = factory_setup
    user = User.objects.get(pk=user.pk)
    serializer = UserSerializer(user)
    url = '/users/%s' % user.pk
    request = factory.get(url)
    force_authenticate(request, user=user, token=token)
    response = user_detail_view(request, str(user.pk))
    assert response.data == serializer.data
    assert response.status_code == status.HTTP_200_OK


def get_json_api_object(type, values=False, with_id=False):
    json_api_obj = '{"data": {"type": "%s"' % type
    if with_id:
        json_api_obj += ',"id": "1"'

    if values:
        json_api_obj += ',"attributes": {"name": "%s", "email": "%s", \
            "password": "%s"}' % (values[0], values[1], values[2])

    json_api_obj += '}}'
    return json_api_obj


@pytest.mark.django_db
def test_post_user(factory_setup):
    factory, user, payload, token = factory_setup
    data = get_json_api_object(
        'create_user',
        ['admin', 'admin@admin.com', 'admin']
    )
    request = factory.post(
        '/create_user/', data,
        content_type='application/vnd.api+json'
    )
    response = create_user(request)
    assert response.status_code == status.HTTP_201_CREATED
    user = User.objects.get(pk=2)
    serializer = UserSerializer(user)
    assert response.data == serializer.data


@pytest.mark.django_db
def test_post_user_empty_name(factory_setup):
    factory, user, payload, token = factory_setup
    data = get_json_api_object('create_user', ['', 'admin@admin.com', 'admin'])
    request = factory.post(
        '/create_user/', data,
        content_type='application/vnd.api+json'
    )
    response = create_user(request)
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert response.data == {"name": ["This field may not be blank."]}


@pytest.mark.django_db
def test_post_user_empty_email(factory_setup):
    factory, user, payload, token = factory_setup
    data = get_json_api_object('create_user', ['admin', '', 'admin'])
    request = factory.post(
        '/create_user/', data,
        content_type='application/vnd.api+json'
    )
    response = create_user(request)
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert response.data == {"email": ["This field may not be blank."]}


@pytest.mark.django_db
def test_post_user_empty_password(factory_setup):
    factory, user, payload, token = factory_setup
    data = get_json_api_object(
        'create_user',
        ['admin', 'admin@admin.com', ' ']
    )
    request = factory.post(
        '/create_user/', data,
        content_type='application/vnd.api+json'
    )
    response = create_user(request)
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert response.data == {"password": ["This field may not be blank."]}


@pytest.mark.django_db
def test_post_user_without_attrs(factory_setup):
    factory, user, payload, token = factory_setup
    data = get_json_api_object('create_user')
    request = factory.post(
        '/create_user/', data,
        content_type='application/vnd.api+json'
    )
    response = create_user(request)
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert response.data['name'] == ['This field is required.']
    assert response.data['email'] == ['This field is required.']
    assert response.data['password'] == ['This field is required.']


@pytest.mark.django_db
def test_update_user(factory_setup, user_detail_view):
    factory, user, payload, token = factory_setup
    data = get_json_api_object(
        'usersDetails',
        ['teste', 'teste@teste.com', 'teste'],
        with_id=True
    )
    url = "/users/%s" % user.pk
    request = factory.patch(
        url, data,
        content_type='application/vnd.api+json'
    )
    force_authenticate(request, user=user, token=token)
    response = user_detail_view(request, str(user.pk))
    user = User.objects.get(pk=user.pk)
    serializer = UserSerializer(user)
    assert response.status_code == status.HTTP_201_CREATED
    assert response.data == serializer.data


@pytest.mark.django_db
def test_update_user_empty_name(factory_setup, user_detail_view):
    factory, user, payload, token = factory_setup
    data = get_json_api_object(
        'usersDetails',
        ['', 'teste@teste.com', 'teste'],
        with_id=True
    )
    url = "/users/%s" % user.pk
    request = factory.patch(
        url, data,
        content_type='application/vnd.api+json'
    )
    force_authenticate(request, user=user, token=token)
    response = user_detail_view(request, str(user.pk))
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert response.data == {"name": ["This field may not be blank."]}


@pytest.mark.django_db
def test_update_user_empty_email(factory_setup, user_detail_view):
    factory, user, payload, token = factory_setup
    data = get_json_api_object(
        'usersDetails',
        ['teste', '', 'teste'],
        with_id=True
    )
    url = "/users/%s" % user.pk
    request = factory.patch(
        url, data,
        content_type='application/vnd.api+json'
    )
    force_authenticate(request, user=user, token=token)
    response = user_detail_view(request, str(user.pk))
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert response.data == {"email": ["This field may not be blank."]}


@pytest.mark.django_db
def test_update_user_empty_password(factory_setup, user_detail_view):
    factory, user, payload, token = factory_setup
    data = get_json_api_object(
        'usersDetails',
        ['teste', 'teste@teste.com', ''],
        with_id=True
    )
    url = "/users/%s" % user.pk
    request = factory.patch(
        url, data,
        content_type='application/vnd.api+json'
    )
    force_authenticate(request, user=user, token=token)
    response = user_detail_view(request, str(user.pk))
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert response.data == {"password": ["This field may not be blank."]}


@pytest.mark.django_db
def test_update_user_with_diffent_id(factory_setup, user_detail_view):
    factory, user, payload, token = factory_setup
    data = get_json_api_object(
        'usersDetails',
        values=False,
        with_id=True
    )
    url = "/users/2"
    request = factory.patch(
        url, data,
        content_type='application/vnd.api+json'
    )
    force_authenticate(request, user=user, token=token)
    response = user_detail_view(request, "2")
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert response.data == messages.PARAMETER_NOT_CORRESPONDING
