from __future__ import unicode_literals
from django.contrib.auth import get_user_model
from django.http import Http404
from django.utils.translation import ugettext as _
from rest_framework.decorators import api_view, authentication_classes
from rest_framework.decorators import permission_classes
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import UserSerializer
from pentano import messages

User = get_user_model()


@api_view(['GET'])
def get_users(request):
    serializer_class = UserSerializer
    users = User.objects.all()
    serializer = serializer_class(users, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def create_user(request):
    serializer_class = UserSerializer
    serializer = serializer_class(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(
            serializer.data,
            status=status.HTTP_201_CREATED
        )
    return Response(
        serializer.errors,
        status=status.HTTP_422_UNPROCESSABLE_ENTITY
    )


class UserDetailView(APIView):
    resource_name = 'usersDetails'
    serializer = UserSerializer

    def get(self, request, user_id):
        user = self.retrieve_user(user_id)
        serializer = self.serializer(user)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, user_id, format=None):
        if(user_id == request.data['id']):
            user = self.retrieve_user(user_id)
            serializer = self.serializer(user, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(
                    serializer.data,
                    status=status.HTTP_201_CREATED
                )
            return Response(
                serializer.errors,
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )
        return Response(
            _(messages.PARAMETER_NOT_CORRESPONDING),
            status=status.HTTP_422_UNPROCESSABLE_ENTITY
        )

    def retrieve_user(self, user_id):
        try:
            user = User.objects.get(pk=user_id)
            return user
        except User.DoesNotExist:
            raise Http404
