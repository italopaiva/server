from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token
from . import views

urlpatterns = [
    url(r'^create_user/$', views.create_user, name='create_user'),
    url(r'^users/$', views.get_users, name='users'),
    url(r'^users/(?P<user_id>[0-9]+)$', views.UserDetailView.as_view(),
        name='user'),
    url(r'^login/', obtain_jwt_token)
]
