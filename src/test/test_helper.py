from django.conf import settings
from django.db.models import signals
from django.apps import apps
import pytest
import importlib
from model_mommy import mommy


"""
 This file contains helper functions and fixtures to be used
 in all other tests.

 By default, all signals are disabled to speed up the run
 To activate signals in a specific test just import
 the functions of this file and mark your test to use
 the connect_signals fixture

 @pytest.mark.usefixtures("connect_signals")
 def test_should_test_something():
    assert True

"""

# TODO
# 1 - refactor app and module name 
# 2 - receive parms from which signals to connect and disconnect
@pytest.fixture(scope="session", autouse=True)
def disconnect_signals():
    for signal, attrs in settings.SIGNALS.items():
        signal_object = getattr(signals, attrs['type'])
        signal_object.disconnect(
            sender=get_model(attrs),
            dispatch_uid=attrs['uid'],
        )


@pytest.fixture(scope="session")
def connect_signals():
    for signal, attrs in settings.SIGNALS.items():
        signal_object = getattr(signals, attrs['type'])
        signal_object.connect(
            get_receiver(signal, attrs),
            sender=get_model(attrs),
            dispatch_uid=attrs['uid'],
        )


def get_receiver(signal, attrs):
    app = attrs['source'].split('/')[0]
    module = importlib.import_module(app + '.signals')
    return getattr(module, signal)


def get_model(attrs):
    model = apps.get_model(
        app_label=attrs['app'], model_name=attrs['class']
    )
    return model
